package com.cw.utils;

import com.cw.entity.User;
import com.cw.ui.ChatUI;
import com.cw.ui.UserUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户数据
 */
public class UserDataUtils {
    public static Map<User,ChatUI> chatUIs = new HashMap<>();
    public static UserUI userUI;
    public static User userData ;
    public static List<User> friendsData;
    public static Map<User, ArrayList<String>> chatInfo = new HashMap<User, ArrayList<String>>();

    private UserDataUtils() {
        throw new RuntimeException("用户数据拒绝访问构造方法");
    }
}
