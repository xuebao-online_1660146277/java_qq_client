package com.cw.net.udp;

import com.cw.net.http.HttpURL;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * 类UDPReceiveT:接收线程
 * @author: chenWei
 * @email: 1660146277@qq.com
 * @date: 2021/5/19
 */
public class UDPReceiveT implements Runnable{
    private DatagramSocket socket = null;
    private String formName;//消息来源
    private Integer openPort;//端口
    private boolean login = true;//登录状态

    public UDPReceiveT() {
        this.formName = "未知";
        this.openPort = Integer.valueOf(HttpURL.config.getString("openPort"));
    }

    @Override
    public void run() {
        try {
            //创建socket并开放端口
            socket = new DatagramSocket(this.openPort);
            //如果状态为登录 循环阻塞接收
            while (login) {
                //准备一个接收数据的数据包 并设定最大的包的大小
                byte[] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);
                socket.receive(packet);
                //获得消息
                String info = new String(packet.getData()).trim().split("@#", 2)[0];
                if (!login){
                    System.out.println("udp关闭成功");
                    break;
                }
                //更新来源
                formName = new String(packet.getData()).trim().split("@#", 2)[1];
                //System.out.println(formName + " : " + info);
                UDPNewsController newsController = new UDPNewsController(info,formName);
                Thread threadNewsController = new Thread(newsController);
                threadNewsController.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public void setSocket(DatagramSocket socket) {
        this.socket = socket;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public Integer getOpenPort() {
        return openPort;
    }

    public void setOpenPort(Integer openPort) {
        this.openPort = openPort;
    }
}
