package com.cw.net.udp;

import com.cw.entity.User;
import com.cw.entity.dto.NewsDTO;
import com.cw.ui.ChatUI;
import com.cw.ui.LoginUI;
import com.cw.ui.UserUI;
import com.cw.utils.JSONUtils;
import com.cw.utils.UserDataUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UDPNewsController implements Runnable{
    private NewsDTO news;
    private String infoFrom;
    @Override
    public void run() {
        if ("%上线通知%".equals(news.getNewsType())) {
            for (HashMap<User, Label> friendLabel : UserUI.friendLabels) {
                String friendName = ((User)friendLabel.keySet().toArray()[0]).getName();
                if (infoFrom.equals(friendName)){
                    Label label = friendLabel.get((User) friendLabel.keySet().toArray()[0]);
                    Display.getDefault().syncExec(new Runnable(){
                        public void run() {
                            label.setText("在线");
                        }
                    });

                }
            }
        }else if ("%离线通知%".equals(news.getNewsType())) {
            for (HashMap<User, Label> friendLabel : UserUI.friendLabels) {
                String friendName = ((User)friendLabel.keySet().toArray()[0]).getName();
                if (infoFrom.equals(friendName)){
                    Label label = friendLabel.get((User) friendLabel.keySet().toArray()[0]);
                    Display.getDefault().syncExec(new Runnable(){
                        public void run() {
                            label.setText("离线");
                        }
                    });

                }
            }
        }else {
            //缓存数据
            Map<User, ArrayList<String>> chatInfo = UserDataUtils.chatInfo;
            ArrayList<String> info = new ArrayList<>();
            User friend = null;
            for (User user : chatInfo.keySet()) {
                if (user.getName().equals(infoFrom)){
                    info = chatInfo.get(user);
                    info.add("    "+news.getNewsData()+"@#=="+infoFrom+"说== : ");
                    friend = user;
                }
            }
            //如果聊天窗口已打开,更新聊天窗口
            ChatUI chatUI = UserDataUtils.chatUIs.get(friend);
            if (chatUI != null){
                ArrayList<String> finalInfo = info;
                Display.getDefault().syncExec(new Runnable(){
                    public void run() {
                        chatUI.getTxtInfo().setText("");
                        for (String s : finalInfo) {
                            if (chatUI.getTxtInfo() == null || "".equals(chatUI.getTxtInfo().getText())) {
                                chatUI.getTxtInfo().setText(s.split("@#", 2)[1] + "\r\n" +
                                        s.split("@#", 2)[0] + "\r\n");
                            } else {
                                chatUI.getTxtInfo().setText(chatUI.getTxtInfo().getText() + (s.split("@#", 2)[1] + "\r\n" +
                                        s.split("@#", 2)[0] + "\r\n"));
                            }
                        }
                    }
                });
            }

        }
    }

    @Override
    public String toString() {
        return "UDPController{" +
                "news=" + news +
                '}';
    }

    public String getInfoFrom() {
        return infoFrom;
    }

    public void setInfoFrom(String infoFrom) {
        this.infoFrom = infoFrom;
    }

    public NewsDTO getNews() {
        return news;
    }

    public void setNews(NewsDTO news) {
        this.news = news;
    }

    public UDPNewsController() {
    }

    public UDPNewsController(String info,String infoFrom) {
        this.infoFrom = infoFrom;
        this.news= JSONUtils.JSON_UTILS.getObject(info, NewsDTO.class);
    }
}
