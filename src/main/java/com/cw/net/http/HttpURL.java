package com.cw.net.http;

import java.util.ResourceBundle;

public class HttpURL {

    public final static ResourceBundle config = ResourceBundle.getBundle("qq-config");
    public final static String HTTP_LOGIN = config.getString("HTTP_LOGIN");
    public final static String HTTP_OFFLINE = config.getString("HTTP_OFFLINE");
    public final static String HTTP_INFO = config.getString("HTTP_INFO");

    private HttpURL(){
        throw new RuntimeException("常量类型,拒绝访问构造方法--陈炜");
    }
}
