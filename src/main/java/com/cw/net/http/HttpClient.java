package com.cw.net.http;

import com.cw.utils.JSONUtils;
import okhttp3.*;

import java.io.IOException;

public enum HttpClient {
    HTTP_CLIENT;
    public final OkHttpClient client = new OkHttpClient.Builder().cookieJar(new CookieManager()).build();
    public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public String post(String httpURL, Object data) throws IOException {
        String json = JSONUtils.JSON_UTILS.getString(data);
        RequestBody body = RequestBody.create(JSON,json);

        Request request = new Request.Builder().url(httpURL).post(body).build();
        Response response = client.newCall(request).execute();
        if (response.body() != null) {
            return response.body().string();
        }else {
            throw new RuntimeException("异常,请检查网络是否稳定");
        }
    }
}
