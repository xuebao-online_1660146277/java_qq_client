package com.cw.entity;

public class Friend {
    private Long id;

    private Long userId;

    private Integer friendId;

    public Friend(Long id, Long userId, Integer friendId) {
        this.id = id;
        this.userId = userId;
        this.friendId = friendId;
    }

    public Friend() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getFriendId() {
        return friendId;
    }

    public void setFriendId(Integer friendId) {
        this.friendId = friendId;
    }
}