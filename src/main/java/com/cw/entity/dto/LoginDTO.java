package com.cw.entity.dto;

import com.cw.entity.User;

import java.util.List;

/**
 * 登录DTO
 */
public class LoginDTO {
    //是否登录成功
    private String loginSuccess;
    //好友列表
    private List<User> friends;
    //用户信息
    private User user;

    @Override
    public String toString() {
        return "LoginDTO{" +
                "loginSuccess='" + loginSuccess + '\'' +
                ", friends=" + friends +
                ", user=" + user +
                '}';
    }

    public String getLoginSuccess() {
        return loginSuccess;
    }

    public void setLoginSuccess(String loginSuccess) {
        this.loginSuccess = loginSuccess;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LoginDTO() {
    }

    public LoginDTO(String loginSuccess, List<User> friends, User user) {
        this.loginSuccess = loginSuccess;
        this.friends = friends;
        this.user = user;
    }
}
