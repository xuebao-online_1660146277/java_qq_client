package com.cw.entity.dto;

import com.cw.entity.vo.NewsVO;
import com.cw.utils.JSONUtils;

public class NewsDTO {
    private String newsType;//消息类型
    private String newsData;//消息内容

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getNewsData() {
        return newsData;
    }

    public void setNewsData(String newsData) {
        this.newsData = newsData;
    }

    @Override
    public String toString() {
        return "NewsDTO{" +
                "newsType='" + newsType + '\'' +
                ", newsData='" + newsData + '\'' +
                '}';
    }

    public NewsDTO() {
    }

    public NewsDTO(String newsType, String newsData) {
        this.newsType = newsType;
        this.newsData = newsData;
    }

//    public static void main(String[] args) {
//        System.out.println(JSONUtils.JSON_UTILS.getString(new NewsVO()));
//    }
}
