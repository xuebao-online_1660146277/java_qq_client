package com.cw.entity.dto;

/**
 * 通知
 */
public class NoticeDTO {
    @Override
    public String toString() {
        return "NoticeDTO{" +
                "notice='" + notice + '\'' +
                '}';
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public NoticeDTO() {
    }

    public NoticeDTO(String notice) {
        this.notice = notice;
    }

    private String notice;
}
