package com.cw.entity.vo;

import com.cw.entity.dto.NewsDTO;

public class NewsVO {
    private NewsDTO info = new NewsDTO("消息", "");
    private String userAccounts;
    private String friendAccounts;

    @Override
    public String toString() {
        return "NewsVO{" +
                "info=" + info.toString() +
                ", userName='" + userAccounts + '\'' +
                ", friendName='" + friendAccounts + '\'' +
                '}';
    }

    public NewsDTO getInfo() {
        return info;
    }

    public void setInfo(NewsDTO info) {
        this.info = info;
    }

    public String getUserAccounts() {
        return userAccounts;
    }

    public void setUserAccounts(String userAccounts) {
        this.userAccounts = userAccounts;
    }

    public String getFriendAccounts() {
        return friendAccounts;
    }

    public void setFriendAccounts(String friendAccounts) {
        this.friendAccounts = friendAccounts;
    }

    public NewsVO() {
    }

    public NewsVO(NewsDTO info, String userAccounts, String friendAccounts) {
        this.info = info;
        this.userAccounts = userAccounts;
        this.friendAccounts = friendAccounts;
    }

}
