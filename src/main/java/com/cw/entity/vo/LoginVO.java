package com.cw.entity.vo;

/**
 * 登录VO
 */
public class LoginVO {
    private String accounts;

    @Override
    public String toString() {
        return "LoginVO{" +
                "accounts='" + accounts + '\'' +
                ", password='" + password + '\'' +
                ", urlPort='" + urlPort + '\'' +
                '}';
    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrlPort() {
        return urlPort;
    }

    public void setUrlPort(String urlPort) {
        this.urlPort = urlPort;
    }

    public LoginVO() {
    }

    public LoginVO(String accounts, String password, String urlPort) {
        this.accounts = accounts;
        this.password = password;
        this.urlPort = urlPort;
    }

    private String password;
    private String urlPort;
}
