package com.cw.entity;

public class User {
    private Long id;

    private String accounts;

    private String password;

    private String name;

    private String icon;

    private String urlPort;

    public User(Long id, String accounts, String password, String name, String icon, String urlPort) {
        this.id = id;
        this.accounts = accounts;
        this.password = password;
        this.name = name;
        this.icon = icon;
        this.urlPort = urlPort;
    }

    public User() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts == null ? null : accounts.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public String getUrlPort() {
        return urlPort;
    }

    public void setUrlPort(String urlPort) {
        this.urlPort = urlPort == null ? null : urlPort.trim();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", accounts='" + accounts + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", urlPort='" + urlPort + '\'' +
                '}';
    }
}