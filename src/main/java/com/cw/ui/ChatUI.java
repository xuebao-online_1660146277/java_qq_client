package com.cw.ui;

import com.cw.entity.User;
import com.cw.entity.dto.NewsDTO;
import com.cw.entity.dto.NoticeDTO;
import com.cw.entity.vo.LoginVO;
import com.cw.entity.vo.NewsVO;
import com.cw.net.http.HttpClient;
import com.cw.net.http.HttpURL;
import com.cw.utils.JSONUtils;
import com.cw.utils.UserDataUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import swt.SWTResourceManager;

import java.io.IOException;
import java.util.List;

public class ChatUI extends Shell {
    private Text txtInfo;
    private Text textTo;
    private User friend;
    private List<String> info;

    /**
     * Launch the application.
     *
     * @param args
     */
    public static void main(String args[]) {
        try {
            Display display = Display.getDefault();
            ChatUI shell = new ChatUI(display, new User());
            shell.open();
            shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the shell.
     *
     * @param display
     */
    public ChatUI(Display display, User friend) {
        super(display, SWT.SHELL_TRIM);
        this.friend = friend;
        this.info = UserDataUtils.chatInfo.get(friend);
        UserDataUtils.chatUIs.put(friend,this);

        setText(friend.getName());
        setLayout(new GridLayout(1, false));

        Composite composite = new Composite(this, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

        txtInfo = new Text(composite, SWT.READ_ONLY | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL);
        txtInfo.setToolTipText("聊天窗口");
        txtInfo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        txtInfo.setForeground(SWTResourceManager.getColor(SWT.COLOR_INFO_FOREGROUND));
        txtInfo.setFont(SWTResourceManager.getFont("宋体", 14, SWT.BOLD));
        txtInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

        Composite composite_1 = new Composite(this, SWT.NONE);
        composite_1.setLayout(new GridLayout(2, false));
        GridData gd_composite_1 = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        gd_composite_1.heightHint = 80;
        composite_1.setLayoutData(gd_composite_1);

        textTo = new Text(composite_1, SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL);
        textTo.setFont(SWTResourceManager.getFont("宋体", 14, SWT.NORMAL));
        textTo.setToolTipText("发送窗口");
        textTo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
        textTo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));


        Button btnOK = new Button(composite_1, SWT.NONE);
        GridData gd_btnOK = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_btnOK.widthHint = 100;
        btnOK.setLayoutData(gd_btnOK);
        btnOK.setText("发送");

        for (String s : info) {
            if (txtInfo.getText() == null || "".equals(txtInfo.getText())) {
                txtInfo.setText(s.split("@#", 2)[1] + "\r\n" +
                        s.split("@#", 2)[0] + "\r\n");
            } else {
                txtInfo.setText(txtInfo.getText() + (s.split("@#", 2)[1] + "\r\n" +
                        s.split("@#", 2)[0] + "\r\n"));
            }
        }
        btnOK.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                String newsData = textTo.getText();
                if (newsData != null && !"".equals(newsData)) {
                    try {
                        String responseJSON = HttpClient.HTTP_CLIENT.post(HttpURL.HTTP_INFO, new NewsVO(
                                new NewsDTO("消息", newsData),
                                UserDataUtils.userData.getAccounts(),
                                ChatUI.this.friend.getAccounts()
                        ));
                        NoticeDTO noticeDTO = JSONUtils.JSON_UTILS.getObject(responseJSON, NoticeDTO.class);
                        if (noticeDTO != null && "发送成功".equals(noticeDTO.getNotice())){
                            info.add("    "+newsData+"@#==你说== : ");
                            txtInfo.setText("");
                            for (String s : info) {
                                if (txtInfo.getText() == null || "".equals(txtInfo.getText())) {
                                    txtInfo.setText(s.split("@#", 2)[1] + "\r\n" +
                                            s.split("@#", 2)[0] + "\r\n");
                                } else {
                                    String old = txtInfo.getText();
                                    txtInfo.setText( (old + s.split("@#", 2)[1] + "\r\n" +
                                            s.split("@#", 2)[0] + "\r\n"));
                                }
                            }
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }

            }
        });
        createContents();
    }

    /**
     * Create contents of the shell.
     */
    protected void createContents() {
        setSize(550, 400);

    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }

    public Text getTxtInfo() {
        return txtInfo;
    }

    public void setTxtInfo(Text txtInfo) {
        this.txtInfo = txtInfo;
    }

    public Text getTextTo() {
        return textTo;
    }

    public void setTextTo(Text textTo) {
        this.textTo = textTo;
    }

    public User getFriend() {
        return friend;
    }

    public void setFriend(User friend) {
        this.friend = friend;
    }

    public List<String> getInfo() {
        return info;
    }

    public void setInfo(List<String> info) {
        this.info = info;
    }
}
