package com.cw.ui;

import com.cw.entity.User;
import com.cw.entity.vo.LoginVO;
import com.cw.net.http.HttpClient;
import com.cw.net.http.HttpURL;
import com.cw.net.udp.UDPReceiveT;
import com.cw.utils.UserDataUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import swt.SWTResourceManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserUI extends Shell {
    public static List<HashMap<User,Label>> friendLabels = new ArrayList<>();
    /**
     * Launch the application.
     * @param args
     */
    public static void main(String args[]) {
        try {
            Thread udp = new Thread(new UDPReceiveT());
            udp.start();
            UserDataUtils.userData = new User(
                    1L,"admin","**","陈炜1号","暂无","127.0.0.1:7070");
            UserDataUtils.friendsData = (new ArrayList<>());
            UserDataUtils.friendsData.add(new User(
                    1L,"admin2","**","陈炜2号","暂无","127.0.0.1:7071")
            );
            Display display = Display.getDefault();
            UserUI shell = new UserUI(display);
            shell.open();
            shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the shell.
     * @param display
     */
    public UserUI(Display display) {
        super(display, SWT.SHELL_TRIM);
        UserDataUtils.userUI = this;
        addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent arg0) {
                LoginUI.udpReceiveT.setLogin(false);
                String responseJSON = "";
                try {
                    responseJSON = HttpClient.HTTP_CLIENT.post(HttpURL.HTTP_OFFLINE, new LoginVO(

                            UserDataUtils.userData.getAccounts(),
                            UserDataUtils.userData.getPassword(),
                            UserDataUtils.userData.getUrlPort()
                            ));
                    System.out.println(responseJSON);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        setText("qq2021");
        setImage(new Image(UserUI.this.getDisplay(),"./image/log.PNG"));
        GridLayout gridLayout = new GridLayout(1, false);
        gridLayout.verticalSpacing = 0;
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        gridLayout.horizontalSpacing = 0;
        setLayout(gridLayout);

        Composite composite = new Composite(this, SWT.NONE);
        GridLayout gl_composite = new GridLayout(4, false);
        gl_composite.verticalSpacing = 0;
        gl_composite.marginWidth = 0;
        gl_composite.marginHeight = 0;
        gl_composite.horizontalSpacing = 0;
        composite.setLayout(gl_composite);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        gd_composite.heightHint = 80;
        composite.setLayoutData(gd_composite);

        Label lblNewLabel = new Label(composite, SWT.NONE);
        GridData gd_lblNewLabel = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
        gd_lblNewLabel.widthHint = 60;
        lblNewLabel.setLayoutData(gd_lblNewLabel);

        Label labelName = new Label(composite, SWT.NONE);
        labelName.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
        labelName.setFont(SWTResourceManager.getFont("宋体", 12, SWT.ITALIC));
        labelName.setAlignment(SWT.CENTER);
        labelName.setText("陈炜1号");
        GridData gd_labelName = new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1);
        gd_labelName.widthHint = 80;
        labelName.setLayoutData(gd_labelName);
        if (UserDataUtils.userData.getName()!=null){
            labelName.setText(UserDataUtils.userData.getName());
        }

        Label labelImage = new Label(composite, SWT.NONE);
        labelImage.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        labelImage.setFont(SWTResourceManager.getFont("宋体", 10, SWT.NORMAL));
        labelImage.setText("暂无头像");
        if ("暂无".equals(UserDataUtils.userData.getIcon())){
            labelImage.setText("暂无头像");
        }

        Label lblNewLabel_1 = new Label(composite, SWT.NONE);
        GridData gd_lblNewLabel_1 = new GridData(SWT.RIGHT, SWT.FILL, false, true, 1, 1);
        gd_lblNewLabel_1.widthHint = 70;
        lblNewLabel_1.setLayoutData(gd_lblNewLabel_1);

        Label lblNewLabel_2 = new Label(this, SWT.NONE);
        GridData gd_lblNewLabel_2 = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        gd_lblNewLabel_2.heightHint = 20;
        lblNewLabel_2.setLayoutData(gd_lblNewLabel_2);

        Label lblNewLabel_3 = new Label(this, SWT.NONE);
        GridData gd_lblNewLabel_3 = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
        gd_lblNewLabel_3.heightHint = 40;
        lblNewLabel_3.setLayoutData(gd_lblNewLabel_3);
        lblNewLabel_3.setFont(SWTResourceManager.getFont("宋体", 12, SWT.BOLD));
        lblNewLabel_3.setText("好友列表");

        Composite composite_1 = new Composite(this, SWT.NONE);
        composite_1.setLayout(new GridLayout(1, false));
        composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

        for (int i = 0; i < UserDataUtils.friendsData.size(); i++) {
            //获得好友
            User friend = UserDataUtils.friendsData.get(i);
            //展示,加载控件
            Composite composite_2 = new Composite(composite_1, SWT.NONE);
            GridLayout gl_composite_2 = new GridLayout(2, false);
            gl_composite_2.verticalSpacing = 0;
            gl_composite_2.marginWidth = 0;
            gl_composite_2.marginHeight = 0;
            gl_composite_2.horizontalSpacing = 0;
            composite_2.setLayout(gl_composite_2);
            GridData gd_composite_2 = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
            gd_composite_2.heightHint = 80;
            composite_2.setLayoutData(gd_composite_2);

            Label lblNewLabel_4 = new Label(composite_2, SWT.NONE);
            lblNewLabel_4.setFont(SWTResourceManager.getFont("宋体", 12, SWT.BOLD));
            lblNewLabel_4.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
            lblNewLabel_4.setText(friend.getName());

            Label lblNewLabel_5 = new Label(composite_2, SWT.NONE);
            lblNewLabel_5.setFont(SWTResourceManager.getFont("宋体", 12, SWT.ITALIC));
            lblNewLabel_5.setAlignment(SWT.RIGHT);
            if ("离线".equals(friend.getUrlPort())){
                lblNewLabel_5.setText("离线");
            }else {
                lblNewLabel_5.setText("在线");
            }
            lblNewLabel_5.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
            //注册控件双击事件击事件
            lblNewLabel_4.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseDoubleClick(MouseEvent e) {
                    try {
                        Display display = Display.getDefault();
                        ChatUI shell = new ChatUI(display, friend);
                        shell.open();
                        shell.layout();
                        while (!shell.isDisposed()) {
                            if (!display.readAndDispatch()) {
                                display.sleep();
                            }
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
            });

            //将控件地址缓存
            HashMap<User, Label> labelHashMap = new HashMap<>();
            //labelHashMap.put(friend,lblNewLabel_4);
            labelHashMap.put(friend,lblNewLabel_5);
            friendLabels.add(labelHashMap);
        }
//        for (HashMap<String, Label> friendLabel : UserUI.friendLabels) {
//            System.out.println(friendLabel.keySet().toArray()[0]);
//            System.out.println(friendLabel.get(friendLabel.keySet().toArray()[0]));
//        }
        createContents();
    }

    /**
     * Create contents of the shell.
     */
    protected void createContents() {
        setSize(350, 700);

    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }
}
