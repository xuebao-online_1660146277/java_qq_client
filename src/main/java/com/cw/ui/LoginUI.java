package com.cw.ui;

import com.cw.entity.User;
import com.cw.entity.dto.LoginDTO;
import com.cw.entity.vo.LoginVO;
import com.cw.net.http.HttpClient;
import com.cw.net.http.HttpURL;
import com.cw.net.udp.UDPReceiveT;
import com.cw.utils.JSONUtils;
import com.cw.utils.UserDataUtils;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import swt.SWTResourceManager;

import java.io.IOException;
import java.util.ArrayList;

public class LoginUI {
    public static UDPReceiveT udpReceiveT = null;
    protected Shell shlQq;
    private Text textUser;
    private Text textPassword;

    /**
     * Launch the application.
     * @param args
     */
    public static void main(String[] args) {
        try {
            LoginUI window = new LoginUI();
            window.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open the window.
     */
    public void open() {
        Display display = Display.getDefault();
        createContents();
        shlQq.open();
        shlQq.layout();
        while (!shlQq.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
    }

    /**
     * Create contents of the window.
     */
    protected void createContents() {
        shlQq = new Shell();
        shlQq.setSize(340, 400);
        shlQq.setText("QQ2021");
        GridLayout gl_shlQq = new GridLayout(1, false);
        gl_shlQq.verticalSpacing = 0;
        gl_shlQq.marginWidth = 0;
        gl_shlQq.marginHeight = 0;
        shlQq.setLayout(gl_shlQq);

        Composite composite = new Composite(shlQq, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        gd_composite.heightHint = 80;
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, false);
        gl_composite.verticalSpacing = 0;
        gl_composite.marginWidth = 0;
        gl_composite.marginHeight = 0;
        gl_composite.horizontalSpacing = 0;
        composite.setLayout(gl_composite);

        Label label = new Label(composite, SWT.NONE);
        label.setImage(new Image(shlQq.getDisplay(),"./image/qq.PNG"));
        label.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));

        Composite composite_1 = new Composite(shlQq, SWT.NONE);
        composite_1.setLayout(new GridLayout(1, false));
        composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        new Label(composite_1, SWT.NONE);

        Label lblNewLabel = new Label(composite_1, SWT.NONE);
        lblNewLabel.setFont(SWTResourceManager.getFont("宋体", 12, SWT.NORMAL));
        lblNewLabel.setAlignment(SWT.CENTER);
        GridData gd_lblNewLabel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        gd_lblNewLabel.heightHint = 25;
        lblNewLabel.setLayoutData(gd_lblNewLabel);
        lblNewLabel.setText("帐号");

        textUser = new Text(composite_1, SWT.BORDER);
        GridData gd_textUser = new GridData(SWT.CENTER, SWT.FILL, true, false, 1, 1);
        gd_textUser.widthHint = 200;
        gd_textUser.heightHint = 25;
        textUser.setLayoutData(gd_textUser);

        Label label_1 = new Label(composite_1, SWT.NONE);
        label_1.setFont(SWTResourceManager.getFont("宋体", 12, SWT.NORMAL));
        label_1.setText("密码");
        label_1.setAlignment(SWT.CENTER);
        GridData gd_label_1 = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        gd_label_1.heightHint = 25;
        label_1.setLayoutData(gd_label_1);

        textPassword = new Text(composite_1, SWT.BORDER);
        GridData gd_textPassword = new GridData(SWT.CENTER, SWT.FILL, true, false, 1, 1);
        gd_textPassword.heightHint = 25;
        gd_textPassword.widthHint = 200;
        textPassword.setLayoutData(gd_textPassword);

        Button btnLogin = new Button(composite_1, SWT.NONE);
        GridData gd_btnLogin = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
        gd_btnLogin.heightHint = 30;
        gd_btnLogin.widthHint = 160;
        btnLogin.setLayoutData(gd_btnLogin);
        btnLogin.setText("登录");

        btnLogin.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                String user = textUser.getText().trim();
                String password = textPassword.getText().trim();
                try {
                    String responseJSON = HttpClient.HTTP_CLIENT.post(HttpURL.HTTP_LOGIN, new LoginVO(user, password,
                            HttpURL.config.getString("userURL") + ":" +
                                    HttpURL.config.getString("openPort")));
                    //System.out.println(responseJSON);
                    LoginDTO loginDTO = JSONUtils.JSON_UTILS.getObject(responseJSON, LoginDTO.class);
                    if ("登录成功".equals(loginDTO.getLoginSuccess())){
                        //加载数据
                        UserDataUtils.userData = loginDTO.getUser();
                        UserDataUtils.friendsData = loginDTO.getFriends();
                        for (User friendsDatum : UserDataUtils.friendsData) {
                            UserDataUtils.chatInfo.put(friendsDatum,new ArrayList<String>());
                        }
                        shlQq.dispose();
                        //开启接收消息线程
                        try {
                            UDPReceiveT udpReceiveT = new UDPReceiveT();
                            Thread udp = new Thread(udpReceiveT);
                            LoginUI.udpReceiveT = udpReceiveT;
                            udp.start();
                            Display display = Display.getDefault();
                            UserUI shell = new UserUI(display);
                            shell.open();
                            shell.layout();
                            while (!shell.isDisposed()) {
                                if (!display.readAndDispatch()) {
                                    display.sleep();
                                }
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }else {
                        MessageDialog.openError(shlQq, "错误提示", "用户名或密码错误,请重新登录!");
                    }
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

    }

}
